import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navs',
  templateUrl: './navs.component.html',
  styleUrls: ['./navs.component.scss']
})
export class NavsComponent {
      showMenu = false;
      showMenu1 = false;
      showMenu2 = false;
      showMenu3 = false;
      hidden = false;
      showMenu4 = false;

  usuario: String = '';
  area: String = '';
  cargo: String = '';
  nombreMenu: String = '';

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router
    ) {}
    ngOnInit() {
      this.nombreMenu = 'Seguimiento';
      console.log(this.showMenu);
      if (sessionStorage.getItem('datos') == null) {
        this.router.navigate(['./']);
      }
      this.area = JSON.parse(String(sessionStorage.getItem('datos'))).area;
      console.log('json: ', this.area);
      this.cargo = JSON.parse(String(sessionStorage.getItem('datos'))).cargo;
      this.usuario = JSON.parse(String(sessionStorage.getItem('datos'))).apNombres;
      if(this.area != 'MESA DE PARTES'){
        this.nombreMenu = 'Bandeja';
      }
    }

  cerrarSesion(){
    sessionStorage.clear();
    this.router.navigate(['./']);
  }

  ingresar(){
    this.router.navigate(['./verRecibo'])
  }
  VerNotificacion(){
    this.hidden = true;
  }
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
    toggleMenu() {
      this.showMenu = !this.showMenu;
   }
   toggleMenu1() {
    this.showMenu1 = !this.showMenu1;
 }
 toggleMenu2() {
  this.showMenu2 = !this.showMenu2;
}
 toggleMenu3() {
  this.showMenu3 = !this.showMenu3;
}
toggleMenu4(){
  this.showMenu4 = !this.showMenu4;
}
}

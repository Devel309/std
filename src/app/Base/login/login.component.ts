import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { MActualizarContraseñaComponent } from 'src/app/Modal/mActualizarContraseña/mActualizarContraseña.component';
import { MAsignarExpedienteComponent } from 'src/app/Modal/mAsignarExpediente/mAsignarExpediente.component';
import { ServiceService } from 'src/app/Service/service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class loginComponent implements OnInit {

  formGroupParent:any = [];
  hide = true;

  constructor(private messageService: MessageService,
    public dialog: MatDialog,
    private router: Router,
    private fb: FormBuilder,
    private service: ServiceService,
) {
  this.formGroupParent = this.fb.group({
    usuario: ['', Validators.required],
    contrasena: ['', Validators.required],
  });
}

  ngOnInit() {
  }
  /*openDialog(enterAnimationDuration: string, exitAnimationDuration: string): void {
    this.dialog.open(MAsignarExpedienteComponent, {
      disableClose: true,
      width: '25%',
      enterAnimationDuration,
      exitAnimationDuration,

    });
  }*/
  iniciarSesion(){
    if (this.formGroupParent.invalid) {
      this.messageService.add({severity:'error', summary: 'Error', detail: 'Ingresar datos'});
      return ;
    }

    let params: any = {
      "user": this.formGroupParent.controls.usuario.value,
      "password": this.formGroupParent.controls.contrasena.value
    }

    this.service.onSeguridad(params).subscribe(
      (result:any)=>{
        console.log("response LOGIN",result);
        if (result.length > 0) {
          if(result[0].indicador == 0){
            this.openDialog('1000ms', '600ms', result[0].idUsuario);
            console.log("aquii");
          }else{
          sessionStorage.setItem('datos', JSON.stringify(result[0]));
          this.router.navigate(['./main/navs']);
        }
        }else{
          this.messageService.add({severity:'warn', summary: 'Datos incorrectos', detail: 'Revisar su datos'});
        }
    });
  }

  openDialog(enterAnimationDuration: string, exitAnimationDuration: string, element:number): void {
    const dialogRef = this.dialog.open(MActualizarContraseñaComponent, {
      enterAnimationDuration,
      exitAnimationDuration,
      disableClose: true ,
      width: '400px',
      data: {name:element},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed',result);
      if(result=='Guardado'){
        this.formGroupParent.controls.contrasena.setValue('');
        //this.listarExpedient();
      }
    });
  }

}
